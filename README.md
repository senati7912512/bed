# Base y Estructura de Datos

## 1. Libros

[SQL Server 2019](https://drive.google.com/file/d/1S_Bl6PMCy1ki2z1UqAWDnA3AMZ8XdGiA/view?usp=sharing)

[Archivos](https://drive.google.com/file/d/1LceoFh1DSGRpLqYPII2mpA9S4OC8WYWg/view?usp=sharing)

[Programacion Transac con SQL Server](https://drive.google.com/file/d/1Yx9MzK13AMuB6zvf3XUCmyGLqjVhGgXD/view?usp=sharing)

## 2. Archivos para el curso

[Retail](https://drive.google.com/file/d/1dmK-v66MkXAFa1fAikMvbqzshZ7F19EF/view?usp=sharing)

[Retail DWH](https://drive.google.com/file/d/1boe8-5WERmrwdk2JVw81rseL8vWr_Wo_/view?usp=sharing)

## 3. Repaso

[Repaso](https://docs.google.com/document/d/1LIst0LdT8e0ABAMN8j2idgsi7AYOJZvR/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)

## 4. Ejercicios

[Ejercicios](https://drive.google.com/file/d/1a5oZP1rWiI7tJ4-570xRX27JNA3TO-jM/view?usp=sharing)

## 5. Ejecicios Nivel 2

[Ejercicio 2](https://drive.google.com/file/d/1Sr33gMk0exWvqR15Yisw4Bi7EVrReTTD/view?usp=sharing)

## 6. Ejercicios T-SQL

[Ejercicio 3](https://drive.google.com/file/d/1Of_Y2ALXqxSyHu0Ui5vKd29J1koLyfbF/view?usp=sharing)

[Ejercicio 4](https://drive.google.com/file/d/1xslqlVcK4JZHoyW9yezSsOkwJkoAZq-v/view?usp=sharing)

[Ejercicio 5](https://drive.google.com/file/d/1M5dT1OHzqIwAoel3CnW340oxUmFMh6Xs/view?usp=sharing)

## 7. Generando estadisticas con SQL Server

[Backup](https://drive.google.com/file/d/1Qf5vqrlm-Sq7RDnIgctCoOi9Qgup_8L4/view?usp=sharing)

[Manual 01](estadisticas/estadisticas.md)

[Manual 02](estadisticas/estadisticas_1.md)

## 8. Ejercicios de laboratorio

[Ejercicio 01](ejercicio/ejercicio.md)