# Dashboard, Gráficos Estadísticos, Sumario de Sistema/Aplicación con C#, WinForm y SQL Server

## 1. Crear Base de Datos

Para este ejercicio, lo primero que haremos será crear la base de datos, tablas y datos necesarios.

```sql
CREATE DATABASE PRACTICA_DASHBOARD
GO
USE PRACTICA_DASHBOARD
go
--------------------------------------------------
--                 TABLAS  
----------------------------------------------------------------
-------------------//PRODUCTO//---------------------------------
CREATE TABLE CATEGORIAS
(
ID INT IDENTITY (1,1) PRIMARY KEY,
CATEGORIA  NVARCHAR (150)
)
go
CREATE TABLE MARCAS
(
ID INT IDENTITY (1,1) PRIMARY KEY,
MARCA  NVARCHAR (150)
)
go
CREATE TABLE PRODUCTOS
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
ID_CATEGORIA INT,
ID_MARCA INT,
DESCRIPCION NVARCHAR (200),
PRECIO_COMPRA FLOAT,
PRECIO_VENTA FLOAT,
STOCK INT,
ESTADO NVARCHAR (15),
CONSTRAINT FK_CATEGORIA FOREIGN KEY (ID_CATEGORIA) REFERENCES CATEGORIAS(ID),
CONSTRAINT FK_MARCA FOREIGN KEY (ID_MARCA) REFERENCES MARCAS(ID),
)  
go
-------------------//PERSONAS//------------------------------------------------------------------------------
go
CREATE TABLE EMPLEADOS 
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
DNI NVARCHAR (10),
CONTRASEÑA NVARCHAR (100),
ID_ROL INT,
NOMBRES NVARCHAR (150),
APELLIDOS NVARCHAR (150),
TELEFONO NVARCHAR (15),
DIRECCION NVARCHAR (150),
)
go
CREATE TABLE CLIENTES 
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
NOMBRES NVARCHAR (150),
APELLIDOS NVARCHAR (150),
RUC NVARCHAR (30),
TELEFONO NVARCHAR (15),
DIRECCION NVARCHAR (150),
)
go
CREATE TABLE PROVEEDORES 
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
RAZON_SOCIAL NVARCHAR (150),
RUC NVARCHAR (30),
TELEFONO NVARCHAR (15),
DIRECCION NVARCHAR (150),
)
go
-------------------//VENTA//------------------------------------------------------------------------------
CREATE TABLE VENTAS 
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
TIPO_COMPROBANTE NVARCHAR (50),
SERIE_COMPROBANTE NVARCHAR (30),
FECHA DATE,
ID_CLIENTE INT,
ID_EMPLEADO INT,
TOTAL FLOAT,
CONSTRAINT FK_CLIENTE FOREIGN KEY (ID_CLIENTE) REFERENCES CLIENTES(ID),
CONSTRAINT FK_EMPLEADO FOREIGN KEY (ID_EMPLEADO) REFERENCES EMPLEADOS(ID)
)
go

CREATE TABLE DETALLE_VENTA
(
ID INT IDENTITY (1,1) PRIMARY KEY, 
ID_VENTA INT,
ID_PRODUCTO INT,
PRECIO_VENTA FLOAT,
CANTIDAD INT,
SUBTOTAL FLOAT,
CONSTRAINT FK_VENTA FOREIGN KEY (ID_VENTA) REFERENCES VENTAS(ID),
CONSTRAINT FK_PRODUCTO_V FOREIGN KEY (ID_PRODUCTO) REFERENCES PRODUCTOS(ID)
)
go
--INSERCION DE DATOS
INSERT INTO CATEGORIAS VALUES
(' S/C'),/*SIN CATEGORIA*/
('Preescolar'),
('Peluche'),
('Muñeca Fasion'),
('Mascota'),
('Auto'),
('Figura Animada'),
('Lanzadora'),
('Juego de rol'),
('Juego de mesa'),
('Rompecabeza'),
('Pelota'),
('Scooter'),
('Skates'),
('Accesorio')
 go

INSERT INTO MARCAS VALUES
(' S/M'),/*SIN MARCA*/
('Baby Alive'),
('Barbie'),
('Chicas Superpoderosas'),
('Disney Princesa'),
('Disney'),
('Frozen'),
('Lady Bug'),
('Play Doh'),
('Star Wars'),
('Tortugas Ninja'),
('Cars'),
('Dinotrux'),
('Hot Wheels'),
('MatchBox'),
('Nerf'),
('Transformes'),
('Boomco'),
('Avengers'),
('Batman'),
('Dragon Ball'),
('Mattel'),
('Max Steel'),
('Pokemon'),
('Mattel')
go

INSERT INTO EMPLEADOS VALUES 
('78952456','admin',1,'Juan','Algeria Torres','9658457455','Av Argentina 2015'),
('95456585','caja',2,'Rodolfo','Fernandez Ajala','5694555512','Av la Marina'),
('16584555','venta',3,'Mariela','Del lago','9632547852','Av la paz 2015'),
('78952456','',4,'Rafael','Alvarez Nava','9658457455','Av Argentina 2015'),
('78952456','',5,'Yaquelin','Caceres Aragon','9658457455','Av Argentina 2015'),
('78952456','conta',6,'Mario','Capira Noa','9658457455','Av Argentina 2015')

go
SELECT *FROM PRODUCTOS 
INSERT INTO PRODUCTOS VALUES
(	7	,	16	,'	ultra flash x10	',	44	,	43	,	30	,'	Activo'),
(	5	,	14	,'	pack 3	',	34	,	12	,	74	,'	Activo'	),
(	3	,	6	,'	Ariel	',	58	,	30	,	12	,'	Activo'),
(	8	,	22	,'	Cartas Magicas'	,	25	,	40	,	12	,'	Activo'),
(	15	,	1	,'	super cars',	10	,	15	,	20	,'	Activo'	),
(	5	,	14	,'	Azul extremo'	,	12	,	12	,	12	,'	Activo'	),
(	5	,	14	,'	black edition'	,	23	,	43	,	54	,'	Activo'	),
(	5	,	14	,'	4x4 ultimate'	,	65	,	67	,	12	,'	Activo'	),
(	5	,	14	,'	Transformes'	,	34	,	34	,	65	,'	Activo'	),
(	5	,	14	,'	Fire Edition'	,	56	,	23	,	345	,'	Activo'	),
(	3	,	6	,'	Rapuncel'	,	56	,	34	,	434	,'	Activo'),
(	3	,	6	,'	Valiente'	,	54	,	33	,	23	,'	Activo	'),
(	3	,	6	,'	Sirenita'	,	5	,	5	,	45	,'	Activo'	),
(	3	,	6	,'	Anna Frozzen'	,	12	,	4	,	43	,'	Activo'	),
(	3	,	6	,'	Elsa frozen'	,	23	,	23	,	4	,'	Activo'	),
(	7	,	16	,'	Laser Nif'	,	23	,	4	,	54	,'	Activo	'),
(	7	,	16	,'	Batman Cannon'	,	34	,	34	,	3	,'	Activo'),
(	8	,	9	,'	Monopolio PAris'	,	23	,	2	,	32	,'	Activo'	),
(	8	,	9	,'	Ajedrez StarWars'	,	34	,	34	,	43	,'	Activo	'),
(	15	,	21	,'	Goku black'	,	23	,	23	,	23	,'	Activo'	),
(	5	,	12	,'	Franchesco	',	341	,	122	,	12	,'	Activo'	),
(	5	,	12	,'	Rayo Macuin'	,	122	,	122	,	12	,'	Activo	'),
(	4	,	24	,'	Picachu	',	12	,	12	,	12	,'	Activo'	),
(	4	,	24	,'	Estrella'	,	123	,	23	,	34	,'	Activo'	),
(	4	,	24	,'	Dragon'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	Chicorita'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	Bolbason'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	rayxchu'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	Viper'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	Bobofe'	,	123	,	23	,	34	,'	Activo	'),
(	4	,	24	,'	Chaizar'	,	123	,	23	,	34	,'	Activo'	),
(	4	,	24	,'	charmilion'	,	123	,	23	,	34	,'	Activo'	),
(	15	,	12	,'	rayo'	,	123	,	23	,	34	,'	Activo	'),
(	15	,	12	,'	grua	',	123	,	23	,	34	,'	Activo'	),
(	15	,	12	,'	grulla'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	goku'	,	123	,	23	,	34	,'	Activo'),
(	6	,	21	,'	gokublack	',	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	frezzer	',	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	majimnbo'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	Cell'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	pikoro'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	gohan'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	chichi'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	bulma'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	vegeta'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	Trunsk'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	goten'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	Videl'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	krilin'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	yancha'	,	123	,	23	,	34	,'	Activo	'),
(	6	,	21	,'	nro 18'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	roshi'	,	123	,	23	,	34	,'	Activo'	),
(	6	,	21	,'	urnaibaba	',	123	,	23	,	34	,'	Activo'	),
(	8	,	1	,'	damas rosa'	,	123	,	23	,	34	,'	Activo'),
(	8	,	1	,'	mono polis cityt'	,	123	,	23	,	34	,'	Activo'	),
(	11	,	1	,'	futbol	',	234	,	34	,	34	,'	Activo'	),
(	11	,	1	,'	basket'	,	234	,	34	,	34	,'	Activo'	),
(	11	,	1	,'	tennis'	,	234	,	34	,	34	,'	Activo'	),
(	11	,	1	,'	kiko'	,	234	,	34	,	34	,'	Activo'	),
(	13	,	1	,'	chico slim'	,	234	,	34	,	34	,'	Activo'	),
(	13	,	1	,'	caros lfd	',	234	,	34	,	34	,'	Activo'	),
(	12	,	1	,'	rosa 1m	',	234	,	34	,	34	,'	Activo	'),
(	12	,	1	,'	tall da'	,	234	,	34	,	34	,'	Activo'	),
(	1	,	9	,'	laptop'	,	234	,	34	,	34	,'	Activo	'),
(	1	,	9	,'	calcu boy'	,	234	,	34	,	34	,'	Activo'	),
(	1	,	9	,'	origami'	,	234	,	34	,	34	,'	Activo	')
GO
INSERT INTO EMPLEADOS values 
('78952456',	'admin',	1,'	Juan',	'Algeria Torres','	9658457455','	Av Argentina 2015'	),
('	95456585',	'caja	',2	,'Rodolfo',	'Fernandez Ajala','	5694555512','	Av la Marina'	),
('	16584555',	'venta	',3	,'Mariela',	'Del lago','	9632547852','	Av la paz 2015'	),
('	78952456','		4	',null,'Rafael','	Alvarez Nava','	9658457455	','Av Argentina 2015'	)
go

INSERT INTO CLIENTES VALUES 
('Juan','Algeria Torres','9658457455','9658457455','Av Argentina 2015'),
('Rodolfo','Fernandez Ajala','5694555512','9658457455','Av la Marina'),
('Mariela','Del lago','9632547852','9658457455','Av la paz 2015')
GO

INSERT INTO VENTAS VALUES
('factura','6656565','19-08-2012',1,1,525),
('factura','6656565','19-08-2012',2,2,525), 
('factura','6656565','19-08-2012',3,3,525), 
('factura','6656565','19-08-2012',1,4,525), 
('factura','6656565','19-08-2012',2,5,525), 
('factura','6656565','19-08-2012',3,6,525)
GO  

INSERT INTO DETALLE_VENTA VALUES
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(4,1,56,5,894),
(4,5,56,5,894),
(4,6,56,5,894),
(4,7,56,5,894),
(4,8,56,5,894),
(4,8,56,5,894),
(4,8,56,5,894),
(4,8,56,5,894),
(4,8,56,5,894),
(2,1,56,5,894),
(2,2,56,5,894),
(2,3,56,5,894),
(2,4,56,5,894),
(2,5,56,5,894),
(2,6,56,5,894),
(3,6,56,5,894),
(3,2,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894),
(3,3,56,5,894)
go
---AGREGA MAS DATOS A LA TABLA VENTAS Y DETALLE VENTAS 

----PROCEDIMIENTOS ALMACENADOS 
----------------------ESTADISTICAS GENERALES
create proc DashboardDatos
@totVentas float output,
@nprod int output,
@nmarc int output,
@ncateg int output,
@nclient int output,
@nprove int output,
@nemple int output
as
Set @totVentas =(select sum(total)as TotalVentas from VENTAS )
Set @nclient=(select count (ID)  as Clientes from CLIENTES)
Set @nprove =(select count (ID) as Proveedores from PROVEEDORES)
Set @nemple  =(select count (ID)as Empleados from EMPLEADOS )
Set @nprod=(select count (ID) as Productos  from PRODUCTOS)
set @nmarc  = (select count (ID) AS marcas from MARCAS)
set @ncateg  = (select count (ID) AS categorias from CATEGORIAS)
go

----------------------TOP 5 PRODUCTOS PREFERIDOS / MAS VENDIDOS 
create proc ProdPreferidos
as
Select Top 5 C.CATEGORIA+' '+M.MARCA +' '+P.DESCRIPCION as Producto , COUNT(ID_PRODUCTO ) AS nroVentas 
from DETALLE_VENTA 
inner join PRODUCTOS AS P ON P.ID =DETALLE_VENTA.ID_PRODUCTO 
inner join CATEGORIAS as C ON C.ID = P.ID_CATEGORIA 
inner join MARCAS AS M ON M.ID = P.ID_MARCA 
Group by ID_PRODUCTO,P.DESCRIPCION,C.CATEGORIA,M.MARCA    
Order by count(5) desc
go
----------------------CANTIDAD DE PRODUCTOS POR CATEGORIA
create proc ProdPorCategoria
as
select C.CATEGORIA  , COUNT(ID_CATEGORIA ) as nroProductos
from 
productos as P 
inner join CATEGORIAS as C ON C.ID=P.ID_CATEGORIA 
group by C.CATEGORIA
order by count(1) DESC
go
```

## 2. Crear Procedimientos Almacenados

## Estadísticas Generales

Creamos un procedimiento almacenado para sumar el total de las ventas, contar la cantidad de empleados, proveedores, clientes, productos, marcas y categorías.

Importante, necesitamos almacenar el resultado de la sentencia en un parámetro de salida, esto para poder extraer el resultado y mostrarlo en la aplicación, por lo tanto, declaramos un parámetro de salida (output).

```sql
----------------------ESTADISTICAS GENERALES
create proc DashboardDatos
@totVentas float output,
@nprod int output,
@nmarc int output,
@ncateg int output,
@nclient int output,
@nprove int output,
@nemple int output
as
Set @totVentas =(select sum(total)as TotalVentas from VENTAS )
Set @nclient=(select count (ID)  as Clientes from CLIENTES)
Set @nprove =(select count (ID) as Proveedores from PROVEEDORES)
Set @nemple  =(select count (ID)as Empleados from EMPLEADOS )
Set @nprod=(select count (ID) as Productos  from PRODUCTOS)
set @nmarc  = (select count (ID) AS marcas from MARCAS)
set @ncateg  = (select count (ID) AS categorias from CATEGORIAS)
go
```

## Productos Preferidos / Más Vendidos

Ahora creamos un procedimiento almacenado para mostrar los productos preferidos por el público, para esto usaremos la tabla detalle venta. Podemos solo mostrar dos, tres o diez primeros productos preferidos, para esto usamos top y lo ordenamos de manera descendiente.

```sql
------TOP 5 PRODUCTOS PREFERIDOS - MAS VENDIDOS 
create proc ProdPreferidos
as
Select Top 5 C.CATEGORIA+' '+M.MARCA +' '+P.DESCRIPCION as Producto , COUNT(ID_PRODUCTO ) AS nroVentas 
from DETALLE_VENTA 
inner join PRODUCTOS AS P ON P.ID =DETALLE_VENTA.ID_PRODUCTO 
inner join CATEGORIAS as C ON C.ID = P.ID_CATEGORIA 
inner join MARCAS AS M ON M.ID = P.ID_MARCA 
Group by ID_PRODUCTO,P.DESCRIPCION,C.CATEGORIA,M.MARCA    
Order by count(5) desc
go
```

## Productos por Categoría

Ahora contaremos la cantidad de productos que hay en una categoría, el procedimiento es el mismo de la anterior.

```sql
----------------------CANTIDAD DE PRODUCTOS POR CATEGORIA
create proc ProdPorCategoria
as
select C.CATEGORIA  , COUNT(ID_CATEGORIA ) as nroProductos
from 
productos as P 
inner join CATEGORIAS as C ON C.ID=P.ID_CATEGORIA 
group by C.CATEGORIA
order by count(1) DESC
go
```

## 3. Crear Proyecto / Aplicación

Creamos un nuevo proyecto en Visual Studio de tipo Aplicación de Windows Form en C#, en el formulario, agregamos control chart desde el cuadro de herramientas para los productos preferidos y productos por categoría, este control nos permite crear gráficos estadísticos. Finalmente agregamos Controles Labels y Picture Box para mostrar el Sumario General.

![imagen](ejercicio/imagen.png)

Descripción de las propiedades principales de controles de usuario.

Chart Categoría por Producto

```c#
Name = “chartProdxCategoria”;
Serie.Name=”series1″ – Por Defecto
series1.ChartArea = “ChartArea1”;
series1.ChartType = DataVisualization.Charting.SeriesChartType.Bar;
series1.IsValueShownAsLabel = true;
series1.LabelForeColor = MenuHighlight;
series1.Palette = DataVisualization.Charting.ChartColorPalette.SeaGreen;
```

Chart Productos Preferidos

```c#
Name = “chartProdPreferidos”;
Palette = DataVisualization.Charting.ChartColorPalette.SeaGreen;
Series.Name = “Series2”; Por Defecto
series2.ChartType = DataVisualization.Charting.SeriesChartType.Doughnut;
series2.IsValueShownAsLabel = true;
series2.LabelFormat = “0”;
```

Las SERIES se crean y se agregan dependiendo del orden que se agrega el control CHART. Por ejemplo.

Chart1 -> Series1
Chart2 -> Series2
Chart3->Series3
… Sucesivamente

Sin embargo, puede cambiar el nombre de Serie desde Form1.Designer.cs

## 4. Crear Cadena de Conexión

Doble clic en el formulario para crear el evento Load y abrir el código del formulario, importamos la librería data SQL Client para interactuar con la base de datos.

```c#
using System.Data;
using System.Data.SqlClient;
```

Creamos la Cadena de Conexión. Declaramos SQL Command para ejecutar sentencias y SQL Data Reader para leer filas.

```c#
SqlConnection Conexion = new SqlConnection("Server=(local);DataBase=Practica_Dashboard;Integrated Security=true");
SqlCommand cmd;
SqlDataReader dr;
```

## 5. Mostrar Gráfico Estadístico de Productos por Categoría

Declaramos ArrayList para guardar el nombre de categoría y la cantidad de productos que existen en ella.

Creamos un método para el gráfico estadístico de productos por categorías, ejecutamos el procedimiento almacenado ProdPorCategoria a través de SQL Command, luego cargamos los datos del resultado en el grafico productos por categoría (Chart chartProdxCategoria) en los ejes correspondientes (X-Y).

```c#
ArrayList Categoria = new ArrayList();
ArrayList CantProd = new ArrayList();
private void GrafCategorias() {
    cmd = new SqlCommand("ProdPorCategoria", Conexion);
    cmd.CommandType = CommandType.StoredProcedure;
    Conexion.Open();
    dr = cmd.ExecuteReader();
    while (dr.Read()) {
        Categoria.Add(dr.GetString(0));
        CantProd.Add(dr.GetInt32(1));
    }
    chartProdxCategoria.Series[0].Points.DataBindXY(Categoria,CantProd );
    dr.Close();
    Conexion.Close();
}
```

## 6. Mostrar Gráfico Estadístico de Productos Preferidos / Más Vendidos

De la misma manera que lo anterior, declaramos ArrayList para guardar el nombre del producto y la cantidad de productos que existen en ella.

Creamos un método para el gráfico estadístico de los productos preferidos o más vendidos, ejecutamos el procedimiento almacenado ProdPreferidos a través de SQL Command, luego cargamos los datos del resultado en el grafico productos preferidos (Chart-> chartProdPreferidos) en los ejes correspondientes (X-Y).

```c#
ArrayList Producto = new ArrayList();
ArrayList Cant = new ArrayList();
private void ProdPreferidos()
{
    cmd = new SqlCommand("ProdPreferidos", Conexion);
    cmd.CommandType = CommandType.StoredProcedure;
    Conexion.Open();
    dr = cmd.ExecuteReader();
    while (dr.Read())
    {
        Producto.Add(dr.GetString(0));
        Cant.Add(dr.GetInt32(1));
    }
    chartProdPreferidos.Series[0].Points.DataBindXY(Producto,Cant );
    dr.Close();
    Conexion.Close();
}
```

## 7. Mostrar Estadísticas Generales

Ahora mostraremos las estadísticas generales, creamos un nuevo método, ejecutamos el procedimiento almacenado y declaramos SQL Parameters como parámetro el parámetro de salida para todos los parámetros del procedimiento almacenado y agregamos los parámetros a los parámetros del SQL Command. El resultado, agregamos como valor a las etiquetas (Label).

```c#
private void DashboardDatos() {
    cmd = new SqlCommand("DashboardDatos",Conexion );
    cmd.CommandType = CommandType.StoredProcedure;

    SqlParameter total = new SqlParameter("@totVentas", 0);total.Direction = ParameterDirection.Output;
    SqlParameter nprod = new SqlParameter("@nprod", 0); nprod.Direction = ParameterDirection.Output;
    SqlParameter nmarca = new SqlParameter("@nmarc", 0); nmarca.Direction = ParameterDirection.Output;
    SqlParameter ncategora = new SqlParameter("@ncateg", 0); ncategora.Direction = ParameterDirection.Output;
    SqlParameter ncliente = new SqlParameter("@nclient", 0); ncliente.Direction = ParameterDirection.Output;
    SqlParameter nproveedores = new SqlParameter("@nprove", 0); nproveedores.Direction = ParameterDirection.Output;
    SqlParameter nempleados = new SqlParameter("@nemple", 0); nempleados.Direction = ParameterDirection.Output;

    cmd.Parameters.Add(total );
    cmd.Parameters.Add(nprod );
    cmd.Parameters.Add(nmarca );
    cmd.Parameters.Add(ncategora);
    cmd.Parameters.Add(ncliente );
    cmd.Parameters.Add(nproveedores );
    cmd.Parameters.Add(nempleados);

    Conexion.Open();
    cmd.ExecuteNonQuery();

    lblTotalVentas.Text = cmd.Parameters["@totVentas"].Value.ToString();
    lblCantCateg.Text = cmd.Parameters["@ncateg"].Value.ToString();
    lblCAntMarcas.Text = cmd.Parameters["@nmarc"].Value.ToString();
    lblCantProd.Text = cmd.Parameters["@nprod"].Value.ToString();
    lblCantClient.Text = cmd.Parameters["@nclient"].Value.ToString();
    lblCantEmple.Text = cmd.Parameters["@nemple"].Value.ToString();
    lblCantProve.Text = cmd.Parameters["@nprove"].Value.ToString();

    Conexion.Close();
}
```

## Final. Invocar los Métodos desde el Evento Load

Finalmente invocamos los métodos anteriores desde el evento load del formulario.

```c#
private void Form1_Load(object sender, EventArgs e)
{
    GrafCategorias();
    ProdPreferidos();
    DashboardDatos();
}
```

